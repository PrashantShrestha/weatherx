## WeatherX

This is a simple app to show the waether in the current location of the device.


You can look into this project for following things..

1. Xamarin Forms Elements (Label, Image, Button)
2. Xamarin StackLayout
3. A complete architecture implementing MVVM pattern
4. Data Binding
5. Commanding
6. Dependency Injection (nuget package: [Autofac](https://www.nuget.org/packages/Autofac/4.9.0-beta1))
7. HTTP Request (nuget package: [Microsoft.net.Http](https://www.nuget.org/packages/Microsoft.Net.Http/)  [Polly](https://www.nuget.org/packages/Polly))
8. Easy JSON parsing via (nuget package: [Newtosoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/12.0.1-beta1))
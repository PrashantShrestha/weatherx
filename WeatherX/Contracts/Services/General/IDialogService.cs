﻿using System;
using System.Threading.Tasks;

namespace WeatherX.Contracts.Services.General
{
    public interface IDialogService
    {
        Task ShowDialog(string title, string message, string buttonLabel);
        void ShowToast(string message);
    }
}

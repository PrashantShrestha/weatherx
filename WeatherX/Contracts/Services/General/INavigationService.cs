﻿using System;
using System.Threading.Tasks;
using WeatherX.ViewModels.Base;

namespace WeatherX.Contracts.Services.General
{
    public interface INavigationService
    {
        Task InitializeAsync();

        Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase;

        Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase;

        Task NavigateToAsync(Type viewModelType);

        Task ClearbackStack();

        Task NavigateToAsync(Type viewModelType, object parameter);

        Task navigatebackAsync();

        Task RemoveLastFromBackStackAsync();

        Task PopToRootAsync();
    }
}

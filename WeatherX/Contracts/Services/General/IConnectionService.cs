﻿using Plugin.Connectivity.Abstractions;

namespace WeatherX.Contracts.Services.General
{
    public interface IConnectionService
    {
        bool IsConnected { get; }
        event ConnectivityChangedEventHandler ConnectivityChanged;
    }
}

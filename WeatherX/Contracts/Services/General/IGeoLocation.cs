﻿using System;
using System.Threading.Tasks;
using WeatherX.Model;

namespace WeatherX.Contracts.Services.General
{
    public interface IGeoLocation
    {
        Task<Coord> GetCurrentLocationAsync();
    }
}

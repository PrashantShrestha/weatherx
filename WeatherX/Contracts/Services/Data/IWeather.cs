﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherX.Model;

namespace WeatherX.Contracts.Services.Data
{
    public interface IWeather
    {
        Task<CurrentWeatherResponse> GetCurrentWeatherAsync(double latitude, double longitude);
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using WeatherX.Contracts.Services.Data;
using WeatherX.Contracts.Services.General;
using WeatherX.Model;
using WeatherX.ViewModels.Base;
using Xamarin.Forms;

namespace WeatherX.ViewModels
{
    public class WeatherViewModel : ViewModelBase
    {
        private readonly IWeather _weatherService;
        private readonly IGeoLocation _geoLocationService;

        private bool _showIndicator;
        private bool _showData;

        private Coord currentCoord;
        private CurrentWeatherResponse _currentWeather;
        private string _cityName;
        private string _weatherDesciption;

        private string _currentTemperature;
        private string _minimumTemperature;
        private string _maximumTemperature;

        private string _feelsLikeTemperature;
        private int _humidity;
        private string _wind;
        private string _visibility;

        private string _dayOfWeek;
        private string _timeOfDay;

        private string _iconUrlPath;

        public string CityName
        {
            get => _cityName;
            set
            {
                _cityName = value;
                OnPropertyChanged();
            }
        }

        public string WeatherDescription
        {
            get => _weatherDesciption;
            set
            {
                _weatherDesciption = value;
                OnPropertyChanged();
            }
        }

        public string CurrentTemperature
        {
            get => _currentTemperature;
            set
            {
                _currentTemperature = value;
                OnPropertyChanged();
            }
        }

        public string MaximumTemperature
        {
            get => _maximumTemperature;
            set
            {
                _maximumTemperature = value;
                OnPropertyChanged();
            }
        }

        public string MinimumTemperature
        {
            get => _minimumTemperature;
            set
            {
                _minimumTemperature = value;
                OnPropertyChanged();
            }
        }

        public string FeeleLikeTemperature
        {
            get => _feelsLikeTemperature;
            set
            {
                _feelsLikeTemperature = value;
                OnPropertyChanged();
            }
        }

        public int Humidity
        {
            get => _humidity;
            set
            {
                _humidity = value;
                OnPropertyChanged();
            }
        }

        public string Wind
        {
            get => _wind;
            set
            {
                _wind = value;
                OnPropertyChanged();
            }
        }

        public string Visibility
        {
            get => _visibility;
            set
            {
                _visibility = value;
                OnPropertyChanged();
            }
        }

        public string DayOfWeek
        {
            get => _dayOfWeek;
            set
            {
                _dayOfWeek = value;
                OnPropertyChanged();
            }
        }

        public string TimeOfDay
        {
            get => _timeOfDay;
            set
            {
                _timeOfDay = value;
                OnPropertyChanged();
            }
        }

        public bool ShowIndicator
        {
            get => _showIndicator;
            set 
            {
                _showIndicator = value;
                OnPropertyChanged();
            }
        }

        public bool ShowData
        {
            get => _showData;
            set
            {
                _showData = value;
                OnPropertyChanged();
            }
        }

        public string IconUrlPath
        {
            get => _iconUrlPath;
            set
            {
                _iconUrlPath = value;
                OnPropertyChanged();
            }
        }


        public ICommand GetLocationCommand => new Command(GetLocation);

        public WeatherViewModel(IConnectionService connectionService,  INavigationService navigationService, IDialogService dialogService, IWeather weatherService, IGeoLocation geoLocationService) : base(connectionService, navigationService, dialogService)
        {
            _weatherService = weatherService;
            _geoLocationService = geoLocationService;

            _currentWeather = new CurrentWeatherResponse();
        }

        private async void GetLocation() 
        {

            CityName = "WEATHERX";
            ShowIndicator = true;
            ShowData = false;
            currentCoord =  await _geoLocationService.GetCurrentLocationAsync();

            _currentWeather = await _weatherService.GetCurrentWeatherAsync(currentCoord.lat, currentCoord.lon);
            CityName = _currentWeather.name.ToUpper();
            WeatherDescription = String.Join(" ", _currentWeather.weather[0].main.ToCharArray()).ToUpper();
            CurrentTemperature = $"{(int)_currentWeather.main.temp}°";
            MaximumTemperature = $"{(int)_currentWeather.main.temp_max}°";
            MinimumTemperature = $"{(int)_currentWeather.main.temp_min}°";
            FeeleLikeTemperature = $"{(int)((_currentWeather.main.temp + _currentWeather.main.temp_max + _currentWeather.main.temp_min) / 3)}°";
            Humidity = _currentWeather.main.humidity;
            Wind = $"{(_currentWeather.wind.speed)} m/s";
            Visibility = $"{_currentWeather.visibility} m";

            DateTime now = DateTime.Now;
            DayOfWeek = String.Join(" ", now.DayOfWeek.ToString().ToCharArray()).ToUpper();
            TimeOfDay = now.Hour <= 12 && now.Hour >= 5 ? "M O R N I N G" : now.Hour <= 17 ? "A F T E R N O O N" : "N I G H T";

            IconUrlPath = $"https://api.openweathermap.org/img/w/{_currentWeather.weather[0].icon}.png";

            ShowIndicator = false;
            ShowData = true;
        }
    }
}

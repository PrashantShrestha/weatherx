﻿using System;
using Autofac;
using WeatherX.Contracts.Repository;
using WeatherX.Contracts.Services.Data;
using WeatherX.Contracts.Services.General;
using WeatherX.Repository;
using WeatherX.Services.Data;
using WeatherX.Services.General;
using WeatherX.ViewModels;

namespace WeatherX.Bootstrap
{
    public class AppContainer
    {

        private static IContainer _container;

        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            // ViewModels
            builder.RegisterType<WeatherViewModel>();

            // serivces - general
            builder.RegisterType<ConnectionService>().As<IConnectionService>();
            builder.RegisterType<DialogService>().As<IDialogService>();
            builder.RegisterType<NavigationService>().As<INavigationService>();
            builder.RegisterType<GeoLocation>().As<IGeoLocation>();

            // services - data
            builder.RegisterType<WeatherService>().As<IWeather>();

            // general
            builder.RegisterType<GenericRepository>().As<IGenericRepository>();

            _container = builder.Build();
        }

        public static object Resolve(Type typeName)
        {
            return _container.Resolve(typeName);
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}

﻿using System;
namespace WeatherX.Constants
{
    public static class APIConstants
    {
        public const string BaseApiUrl = "https://api.openweathermap.org/";
        public const string CurrentWeatherEndpoint = "data/2.5/weather";
        public const string APIKey = "5477457314f9809c4fe576fb7235f432";
    }
}

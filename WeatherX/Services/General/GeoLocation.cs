﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using WeatherX.Contracts.Services.General;
using WeatherX.Model;

namespace WeatherX.Services.General
{
    public class GeoLocation : IGeoLocation
    {
        public async Task<Coord> GetCurrentLocationAsync()
        {

            var coord = new Coord();
            Position position = null;
            try 
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;
                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    coord.lat = position.Latitude;
                    coord.lon = position.Longitude;
                    Debug.WriteLine(position);
                    return coord;
                } 

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    return null;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10), null, true);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Unable to get location: " + ex);
            }

            if (position == null)
                return null;

            var output = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
                    position.Timestamp, position.Latitude, position.Longitude,
                    position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);

            Debug.WriteLine(output);

            coord.lat = position.Latitude;
            coord.lon = position.Longitude;

            return coord;
        }
    }
}

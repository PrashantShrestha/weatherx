﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherX.Bootstrap;
using WeatherX.Contracts.Services.General;
using WeatherX.ViewModels;
using WeatherX.ViewModels.Base;
using WeatherX.Views;
using Xamarin.Forms;

namespace WeatherX.Services.General
{
    public class NavigationService: INavigationService
    {
        private readonly Dictionary<Type, Type>_mappings;

        protected Application CurrentApplication => Application.Current;

        public NavigationService()
        {
            _mappings = new Dictionary<Type, Type>();

            CreaePageViewModelMappings();
        }

        public async Task InitializeAsync()
        {
            await NavigateToAsync<WeatherViewModel>();
        }

        public async Task ClearbackStack()
        {
            await CurrentApplication.MainPage.Navigation.PopToRootAsync();
        }



        public Task navigatebackAsync()
        {
            throw new NotImplementedException();
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        public Task NavigateToAsync(Type viewModelType)
        {
            return InternalNavigateToAsync(viewModelType, null);
        }

        public Task NavigateToAsync(Type viewModelType, object parameter)
        {
            return InternalNavigateToAsync(viewModelType, parameter);
        }

        public Task PopToRootAsync()
        {
            throw new NotImplementedException();
        }

        public Task RemoveLastFromBackStackAsync()
        {
            throw new NotImplementedException();
        }



        protected virtual async Task InternalNavigateToAsync(Type viewModelType, object parameter)
        {
            Page page = CreateAndBindPage(viewModelType, parameter);

            if (page is WeatherView)
            {
                CurrentApplication.MainPage = page;
            }
            else
            {
                var navigationPage = CurrentApplication.MainPage as AppNavigationPage;

                if (navigationPage != null)
                {
                    await navigationPage.PushAsync(page);
                }
                else
                {
                    CurrentApplication.MainPage = new AppNavigationPage(page);
                }
            }

            await (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
        }

        protected Page CreateAndBindPage(Type viewModelType, object parameter)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);

            if (pageType == null)
            {
                throw new Exception($"Mapping type for {viewModelType} is not a page");
            }

            Page page = Activator.CreateInstance(pageType) as Page;
            ViewModelBase viewModel = AppContainer.Resolve(viewModelType) as ViewModelBase;
            page.BindingContext = viewModel;

            return page;
        }

        protected Type GetPageTypeForViewModel(Type viewModelType)
        {
            if (!_mappings.ContainsKey(viewModelType))
            {
                throw new KeyNotFoundException($"No map for {viewModelType} was found on navigation mappings");
            }

            return _mappings[viewModelType];
        }


        private void CreaePageViewModelMappings()
        {
            _mappings.Add(typeof(WeatherViewModel), typeof(WeatherView));
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using Akavache;
using WeatherX.Constants;
using WeatherX.Contracts.Repository;
using WeatherX.Contracts.Services.Data;
using WeatherX.Model;

namespace WeatherX.Services.Data
{
    public class WeatherService: BaseService, IWeather
    {
        private readonly IGenericRepository _genericRepository;

        public WeatherService(IGenericRepository genericRepository, IBlobCache cache = null): base(cache)
        {
            _genericRepository = genericRepository;
        }

        public async Task<CurrentWeatherResponse> GetCurrentWeatherAsync(double latitude, double longitude)
        {

            UriBuilder builder = new UriBuilder(APIConstants.BaseApiUrl)
            {
                Path = $"{APIConstants.CurrentWeatherEndpoint}",
                Query = $"lat={latitude}&lon={longitude}&units=metric&appid={APIConstants.APIKey}"
            };

            var currentWeather = await _genericRepository.GetAsync<CurrentWeatherResponse>(builder.ToString());

            return currentWeather;
        }
    }
}
